;;; org-grouped-weektree.el --- Group weeks by month or quarter in Org capture date trees  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Jack Kamm

;; Author: Jack Kamm <jackkamm@gmail.com>
;; Keywords: calendar

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; See Readme.org.

;;; Code:
(require 'org-datetree)
(require 'cal-iso)

(defun org-grouped-weektree-capture-month-week ()
  "Function to capture into a month+week datetree.
Weeks are assigned to the month that Thursday is in.  To be used
with a file+function target of an `org-capture-templates'
element."
  (org-grouped-weektree--find-month-week-create
   (calendar-gregorian-from-absolute (org-today))))

(defun org-grouped-weektree-capture-quarter3m-week ()
  "Function to capture into a quarter+week datetree.
Quarters are defined as 3-month periods, and weeks are assigned
to the quarter that Thursday is in.  To be used with a
file+function target of an `org-capture-templates' element."
  (org-grouped-weektree--find-quarter3m-week-create
   (calendar-gregorian-from-absolute (org-today))))

(defun org-grouped-weektree-capture-quarter13w-week ()
  "Function to capture into a quarter+week datetree.
Quarters are defined as 13-week periods, with the final quarter
of a 53-week year having 14 weeks.  To be used with a
file+function target of an `org-capture-templates' element."
  (org-grouped-weektree--find-quarter13w-week-create
   (calendar-gregorian-from-absolute (org-today))))

;; TODO: Add KEEP-RESTRICTION arg to match signature of
;; `org-datetree-find-date-create'
(defun org-grouped-weektree--find-month-week-create (d)
  "Find or create month-week entry for date D."
  (org-grouped-weektree--find-grouped-week-create
   d "^\\*+[ \t]+%d-\\([01][0-9]\\) \\w+$"
   (org-grouped-weektree--iso-date-calendar-month
    (org-grouped-weektree--gregorian-to-iso-week-year d)
    (org-grouped-weektree--gregorian-to-iso-week d))
   nil))

;; TODO: Add KEEP-RESTRICTION arg to match signature of
;; `org-datetree-find-date-create'
(defun org-grouped-weektree--find-quarter3m-week-create (d)
  "Find or create quarter-week entry for date D.
This defines quarters as 3-month periods."
  (let* ((weekyear (org-grouped-weektree--gregorian-to-iso-week-year d))
         (week (org-grouped-weektree--gregorian-to-iso-week d))
         (quarter (org-grouped-weektree--iso-date-calendar-quarter
                   weekyear week)))
    (org-grouped-weektree--find-grouped-week-create
     d "^\\*+[ \t]+%d-Q\\([1-4]\\)$"
     quarter
     (format "%d-Q%d" weekyear quarter))))

;; TODO: Add KEEP-RESTRICTION arg to match signature of
;; `org-datetree-find-date-create'
(defun org-grouped-weektree--find-quarter13w-week-create (d)
  "Find or create quarter-week entry for date D.
This defines quarters as 13-week periods."
  (let* ((weekyear (org-grouped-weektree--gregorian-to-iso-week-year d))
         (week (org-grouped-weektree--gregorian-to-iso-week d))
         (quarter (org-grouped-weektree--iso-date-regular-quarter week)))
    (org-grouped-weektree--find-grouped-week-create
     d "^\\*+[ \t]+%d-Q\\([1-4]\\)$"
     quarter
     (format "%d-Q%d" weekyear quarter))))

(defun org-grouped-weektree--find-grouped-week-create
    (d regex-template group-num insert)
  "Find or create grouped week entries for date D.
REGEX-TEMPLATE and INSERT are passed to
`org-datetree--find-create' for the intermediate
grouping (e.g. month or quarter). GROUP-NUM is number of the
intermediate grouping (e.g. 1 for January or Q1), and is passed
as the month field to `org-datetree--find-create'."
  (let* ((weekyear (org-grouped-weektree--gregorian-to-iso-week-year d))
	 (week (org-grouped-weektree--gregorian-to-iso-week d)))
    ;; TODO Switch to setting MATCH-TITLE in
    ;; org-datetree--find-create. See also:
    ;; 005c9ae7474e8652a628aee392f1bc377eeb15fa
    ;; https://list.orgmode.org/orgmode/87sfghqqpu.fsf@gmail.com/
    (org-datetree--find-create
     "^\\*+[ \t]+\\([12][0-9]\\{3\\}\\)\\(\\s-*?\
\\([ \t]:[[:alnum:]:_@#%%]+:\\)?\\s-*$\\)"
     weekyear)
    (org-datetree--find-create regex-template weekyear group-num nil insert)
    (org-datetree--find-create
     "^\\*+[ \t]+%d-W\\([0-5][0-9]\\)$"
     weekyear group-num week
     (format "%d-W%02d" weekyear week))))

(defun org-grouped-weektree--gregorian-to-iso-week (d)
  "Returns iso-week of date D."
  (nth 0 (calendar-iso-from-absolute (calendar-absolute-from-gregorian d))))

(defun org-grouped-weektree--gregorian-to-iso-week-year (d)
  "Returns year of iso-week of date D."
  (nth 2 (calendar-iso-from-absolute (calendar-absolute-from-gregorian d))))

(defun org-grouped-weektree--iso-date-calendar-month (year week)
  "Return calendar month of Thursday in WEEK of YEAR.
This assigns weeks to months in a manner consistent with the
ISO-8601 mapping of weeks to years."
  (calendar-extract-month
   ;; anchor on Thurs, to be consistent with weekyear
   (calendar-gregorian-from-absolute
    (calendar-iso-to-absolute
     `(,week 4 ,year)))))

(defun org-grouped-weektree--iso-date-calendar-quarter (year week)
  "Return calendar quarter of Thursday in WEEK of YEAR.
This assigns weeks to calendar quarters (3-month periods) in a
manner consistent with the ISO-8601 mapping of weeks to years."
  (1+ (/ (1- (org-grouped-weektree--iso-date-calendar-month year week)) 3)))

(defun org-grouped-weektree--iso-date-regular-quarter (week)
  "Return regular (13-week) quarter of WEEK.
For 53-week years, the final quarter has 14 weeks."
  (min 4 (1+ (/ (1- week) 13))))

(provide 'org-grouped-weektree)
;;; org-grouped-weektree.el ends here
